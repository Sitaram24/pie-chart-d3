import React, { useState, useEffect } from "react";
import * as d3 from "d3";
import './App.css';
import PieHooks from "./PieHook";
function App() {
  const generateData = (value, length = 5) =>
    d3.range(length).map((item, index) => ({
      date: index,
      value: value === null || value === undefined ? Math.random() * 100 : value
    }));

  const [data, setData] = useState(generateData(0));

  useEffect(() => {
    setData(generateData());
  }, [!data]);
  return (
    <div className="App">
        <PieHooks
          data={data}
          width={800}
          height={600}
          innerRadius={70}
          outerRadius={100}
          />
        <div>Tabs</div>
    </div>
  );
}

export default App;
