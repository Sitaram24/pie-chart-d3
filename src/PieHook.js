import React, { useEffect, useRef } from "react";
import * as d3 from "d3";

const Pie = props => {
  const svgRef = useRef(null);
  const createPie = d3
    .pie()
    .value(d => d.value)
    .sort(null);
  const createArc = d3
    .arc()
    .innerRadius(props.innerRadius)
    .outerRadius(props.outerRadius);
  const colors = d3.scaleOrdinal(d3.schemeCategory10);
  //const format = d3.format(".2f");
  useEffect(
    () => {
      const data = createPie(props.data);
      const group = d3.select(svgRef.current);
      const groupWithData = group.selectAll("g.arc").data(data);
      d3.select("svg").style("background","#192626e0")
      groupWithData.exit().remove();

      const groupWithUpdate = groupWithData
        .enter()
        .append("g")
        .attr("class", "arc");

      const path = groupWithUpdate
        .append("path")
        .merge(groupWithData.select("path.arc"));

      path
        .attr("class", "arc")
        .attr("d", createArc)
        .attr("fill", (d, i) => colors(i));

      const line = groupWithUpdate
        .append("line")
        .merge(groupWithData.select("line"))
        
      line
        .attr("x1", 0)
        .attr("y1", 0)
        .attr("x2", (d) => {
            const [x] = createArc.centroid(d);
            return x;
        })
        .attr("y2", (d) => {
          const [,y] = createArc.centroid(d);
          return y;
        })
        // .attr("stroke", ({ data: d }) => d.color)
        .attr("stroke", "white")
        .attr("stroke-width", 2.5)
        .attr("transform", (d) => {
            const [x, y] = createArc.centroid(d);
            return `translate(${x} ${y})`;
        })
        .attr("stroke-dasharray", 0)
        .attr("stroke-dashoffset", 0);

      const text = groupWithUpdate
        .append("text")
        .merge(groupWithData.select("text"));

      text
      .style("fill","white")
      .attr("x", (d) => {
        const [x] = createArc.centroid(d);
        // return x > 0 ? "25" : "-25";
        return x;
    })
      .attr("y", (d) => {
        const [,y] = createArc.centroid(d);
        // return x > 0 ? "25" : "-25";
        return y;
    })
      .attr("font-size", 10)
      .attr("text-anchor", (d) => {
        const [x] = createArc.centroid(d);
        return x > 0 ? "start" : "end";
      })
      .attr("transform", (d) => {
        const [x, y] = createArc.centroid(d);
        const offset = x > 0 ? 15 : -15;
        const offsety = y > 0 ? 15 : -15;
        return `translate(${x * 2 + offset} ${y + offsety})`;
      })
      .html(
        ({ data: e }) => {
          console.log('data')
          console.log(data)
          return `<tspan x="0">${e.value}:</tspan><tspan x="0" dy="10" font-size="6">${e.value}M</tspan>`})
      // .style("opacity", 1)
      // .style("visibility", "visible");

    },
    [props.data]
  );

  return (
    <svg width={props.width} height={props.height}>
      <g
        ref={svgRef}
        transform={`translate(${props.width / 2} ${props.height / 2})`}
      />
    </svg>
  );
};

export default Pie;